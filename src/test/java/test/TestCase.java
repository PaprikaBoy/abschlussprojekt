package test;

import static org.junit.Assert.assertSame;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.axa.ch.its.abschlussprojekt.model.Card;
import com.axa.ch.its.abschlussprojekt.model.Deck;
import com.axa.ch.its.abschlussprojekt.service.CardService;
import com.axa.ch.its.abschlussprojekt.service.DeckOfCardsProxy;

public class TestCase {
	
	private Deck deck;
	private DeckOfCardsProxy proxy;
	
	private CardService testee;
	
	@Before
	public void setUp() throws MalformedURLException {
		deck = new Deck();
		List<Card> cards = new ArrayList<>(); 
		
		Card c1 = new Card();
		c1.setCode("QS");
		c1.setImage(null);
		c1.setShow(true);
		c1.setWeight(12);
		c1.setSuit("SPADES");
		c1.setValue("QUEEN");
		
		cards.add(c1);
		
		deck.setRemaining(0);
		deck.setCards(cards);
		deck.setDeck_id("deckID");
		
		c1 = Mockito.mock(Card.class);
		Mockito.when(c1.getImage()).thenReturn(new URL("/pictures/placeholder"));
		
		proxy = Mockito.mock(DeckOfCardsProxy.class);
		Mockito.when(proxy.getNewDeck()).thenReturn(deck);
		testee = new CardService();
	}
	
	@Test
	public void test() {
		assertSame(testee.getCurrentDeck().getDeck_id(), new Deck().getDeck_id());
	}
}
