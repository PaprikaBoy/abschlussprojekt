package com.axa.ch.its.abschlussprojekt.model;

public interface MyObserver<T> {

	public void update(T model);
}
