package com.axa.ch.its.abschlussprojekt.view;

import com.axa.ch.its.abschlussprojekt.control.MainController;

import javafx.application.Application;
import javafx.stage.Stage;

public class CardGameMain extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		StageLoader gameLoader = new StageLoader("MainView.fxml", "Card Game Main", new MainController());
		gameLoader.loadStage();

	}

	public static void main(String[] args) {
		launch(args);

	}

	public void startGame() {
		StageLoader gameLoader = new StageLoader("MainView.fxml", "Card Game Main", new MainController());
		gameLoader.loadStage();
	}

}
