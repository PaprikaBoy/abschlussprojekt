package com.axa.ch.its.abschlussprojekt.model;

import java.net.URL;

public class Card {
	private URL image;
	private String value;
	private String suit;
	private String code;
	private Images images;

	private boolean shown;
	private int weight;
	private int colorWeight;

	public Card() {
		this.shown = false;
	}

	public Images getImages() {
		return images;
	}

	public void setImages(Images images) {
		this.images = images;
	}

	public URL getImage() {
		return image;
	}

	public void setImage(URL image) {
		this.image = image;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getSuit() {
		return suit;
	}

	public void setSuit(String suit) {
		this.suit = suit;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isShown() {
		return shown;
	}

	public Card setShow(boolean shown) {
		this.shown = shown;
		return this;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getColorWeight() {
		return colorWeight;
	}

	public void setColorWeight(int colorWeight) {
		this.colorWeight = colorWeight;
	}

}
