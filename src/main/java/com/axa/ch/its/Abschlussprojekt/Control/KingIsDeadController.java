package com.axa.ch.its.abschlussprojekt.control;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import com.axa.ch.its.abschlussprojekt.control.logic.KingIsDeadLogic;
import com.axa.ch.its.abschlussprojekt.model.Card;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class KingIsDeadController extends Controller {
	@FXML
	private Button btnBack;
	@FXML
	private Button btnFlipCard;
	@FXML
	private Button btnNewDeck;
	@FXML
	private Button btnConfirm;
	@FXML
	private ComboBox<String> ddAvailableCards;
	@FXML
	private ImageView ivCurrentCard;
	@FXML
	private Label lblCardName;
	@FXML
	private Label lblError;

	private KingIsDeadLogic logic;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		logic = new KingIsDeadLogic();

		// Button actions
		btnBack.setOnAction(super::goBack);
		btnFlipCard.setOnAction(this::flipCard);
		btnConfirm.setOnAction(this::confirmCollection);
		btnNewDeck.setOnAction(this::drawNewDeck);

		// add available Cards
		ddAvailableCards.setItems(addCardsToChoiceBox());

		// set default Background
		ivCurrentCard.setImage(new Image("/pictures/cardBackside.jpg"));

		btnFlipCard.setDisable(true);
	}

	private ObservableList<String> addCardsToChoiceBox() {
		ObservableList<String> list = FXCollections.observableArrayList();

		HashMap<String, String> availableCards = logic.getAvailableCards();
		availableCards.forEach((k, v) -> list.add(v));

		return list;
	}

	private void confirmCollection(ActionEvent event) {
		String selection = ddAvailableCards.getSelectionModel().getSelectedItem();

		if (selection == null)
			lblError.setText("Please choose a Card first");

		else {
			lblError.setText("");
			logic.setSelection(selection);

			// set Button Disable
			ddAvailableCards.setDisable(true);
			btnConfirm.setDisable(true);
			btnFlipCard.setDisable(false);
		}
	}

	private void flipCard(ActionEvent event) {

		try {
			Card card = logic.flipCard();
			Image cardImg = getCardImage(card.getImage());

			lblCardName.setText(card.getValue() + " " + card.getSuit());
			ivCurrentCard.setImage(cardImg);

			if (logic.cardFound()) {
				btnFlipCard.setDisable(true);
				btnConfirm.setDisable(false);

				ddAvailableCards.setDisable(false);
				ddAvailableCards.setItems(addCardsToChoiceBox());

				lblError.setText("Card was found");
			}
		} catch (IndexOutOfBoundsException e) {
			lblError.setText("The game is finished");
		}

	}

	private void drawNewDeck(ActionEvent event) {
		logic.newDeck();

		btnFlipCard.setDisable(true);
		btnConfirm.setDisable(false);
		ddAvailableCards.setDisable(false);

		ivCurrentCard.setImage(new Image("/pictures/cardBackside.jpg"));
		lblError.setText("");
		lblCardName.setText("");

		ddAvailableCards.setItems(addCardsToChoiceBox());

	}

	
}
