package com.axa.ch.its.abschlussprojekt.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.axa.ch.its.abschlussprojekt.model.Card;
import com.axa.ch.its.abschlussprojekt.model.Deck;

public class CardService implements Serializable {
	private static final long serialVersionUID = 1L;

	private DeckOfCardsProxy proxy;

	private Deck currentDeck;

	private int cardIDtoFlip;

	public CardService() {
		proxy = new DeckOfCardsProxy();
		this.getNewDeck();
		cardIDtoFlip = 0;

	}

	public Deck getNewDeck() {
		currentDeck = proxy.getNewDeck();
		this.setCardWeight();
		cardIDtoFlip = 0;

		return currentDeck;
	}

	public DeckOfCardsProxy getProxy() {
		return proxy;
	}

	public void setProxy(DeckOfCardsProxy proxy) {
		this.proxy = proxy;
	}

	public Card flipCard() {
		Card card = currentDeck.getCards().get(cardIDtoFlip);
		cardIDtoFlip++;

		return card;
	}

	public List<Card> getNotFlipedCards() {
		List<Card> list = new ArrayList<>();
		for (int i = 51; i > cardIDtoFlip; i--)
			list.add(currentDeck.getCards().get(i));

		return list;
	}

	public Deck getCurrentDeck() {
		return currentDeck;
	}

	private void setCardWeight() {
		int weight = -1;
		int colorWeight = -1;

		for (Card card : currentDeck.getCards()) {
			try {
				weight = Integer.parseInt(card.getValue());
			} catch (NumberFormatException pe) {
				switch (card.getValue()) {
				case "ACE":
					weight = 1;
					break;
				case "JACK":
					weight = 11;
					break;
				case "QUEEN":
					weight = 12;
					break;
				case "KING":
					weight = 13;
					break;
				default:
					break;
				}

			}			
			
			if (card.getSuit().contains("CLUBS")|| card.getSuit().contains("SPADES"))
				colorWeight = 1;
			else
				colorWeight = 2;

			card.setWeight(weight);
			card.setColorWeight(colorWeight);
		}
	}

}
