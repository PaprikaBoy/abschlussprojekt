package com.axa.ch.its.abschlussprojekt.control.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.axa.ch.its.abschlussprojekt.control.SolitaireController;
import com.axa.ch.its.abschlussprojekt.control.StackModel;
import com.axa.ch.its.abschlussprojekt.model.Card;
import com.axa.ch.its.abschlussprojekt.service.CardService;

import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class SolitaireLogic {
	private CardService service;

	private HashMap<String, StackModel> stacks;

	private String lastStackId;

	private Card cardToRemove;
	private Card flipedCard;
	private Card clickedCard;
	private List<Card> cardsToMove;

	public SolitaireLogic(SolitaireController controller) {
		service = new CardService();

		stacks = new HashMap<>();
		stacks.put("BackStack", new StackModel("BackStack"));
		stacks.put("BackStackFlip", new StackModel("BackStackFlip"));
		stacks.put("FinalStackOne", new StackModel("FinalStackOne"));
		stacks.put("FinalStackTwo", new StackModel("FinalStackTwo"));
		stacks.put("FinalStackThree", new StackModel("FinalStackThree"));
		stacks.put("FinalStackFour", new StackModel("FinalStackFour"));
		stacks.put("MiddleStackOne", new StackModel("MiddleStackOne"));
		stacks.put("MiddleStackTwo", new StackModel("MiddleStackTwo"));
		stacks.put("MiddleStackThree", new StackModel("MiddleStackThree"));
		stacks.put("MiddleStackFour", new StackModel("MiddleStackFour"));
		stacks.put("MiddleStackFive", new StackModel("MiddleStackFive"));
		stacks.put("MiddleStackSix", new StackModel("MiddleStackSix"));
		stacks.put("MiddleStackSeven", new StackModel("MiddleStackSeven"));

		stacks.forEach((k, v) -> v.addObserver(controller));

		lastStackId = "";

	}

	public HashMap<String, StackModel> getStacks() {
		return stacks;
	}

	public Card getFlipedCard() {
		return flipedCard;
	}

	public void startGame() {

		stacks.get("MiddleStackOne").addCard(service.flipCard().setShow(true));

		stacks.get("MiddleStackTwo").addSomeCards(Arrays.asList(service.flipCard(), service.flipCard().setShow(true)));

		stacks.get("MiddleStackThree")
				.addSomeCards(Arrays.asList(service.flipCard(), service.flipCard(), service.flipCard().setShow(true)));

		stacks.get("MiddleStackFour").addSomeCards(Arrays.asList(service.flipCard(), service.flipCard(),
				service.flipCard(), service.flipCard().setShow(true)));

		stacks.get("MiddleStackFive").addSomeCards(Arrays.asList(service.flipCard(), service.flipCard(),
				service.flipCard(), service.flipCard(), service.flipCard().setShow(true)));

		stacks.get("MiddleStackSix").addSomeCards(Arrays.asList(service.flipCard(), service.flipCard(),
				service.flipCard(), service.flipCard(), service.flipCard(), service.flipCard().setShow(true)));

		stacks.get("MiddleStackSeven")
				.addSomeCards(Arrays.asList(service.flipCard(), service.flipCard(), service.flipCard(),
						service.flipCard(), service.flipCard(), service.flipCard(), service.flipCard().setShow(true)));

		stacks.get("BackStack").addSomeCards(service.getNotFlipedCards());

	}

	public void flipCard() {
		try {
			flipedCard = stacks.get("BackStack").getCards().get(stacks.get("BackStack").getCards().size() - 1);

			stacks.get("BackStackFlip").addCard(flipedCard);
			stacks.get("BackStack").removeCard(stacks.get("BackStack").getCards().size() - 1);
		} catch (IndexOutOfBoundsException e) {
			Collections.shuffle(stacks.get("BackStackFlip").getCards());
			stacks.get("BackStack").addSomeCards(stacks.get("BackStackFlip").getCards());

			stacks.get("BackStackFlip").removeAllCards();
			flipedCard = null;
		}
	}

	public String getRemainingBackStack() {
		return String.valueOf("Remaining: " + stacks.get("BackStack").getCards().size());
	}

	public String setFirstClick(MouseEvent event) {
		String source = ((ImageView) event.getSource()).getId();
		String numberParts = this.getNumberParts(source);
		String stackId = null;
		String selectedCard = "";

		if (!numberParts.equals("")) {
			int cardPlace = Integer.valueOf(numberParts);
			stackId = source.substring(0, source.length() - numberParts.length());
			stackId = stackId.substring(2, stackId.length());

			if ((stacks.get(stackId).getCards().size() - 1) > cardPlace) {
				cardsToMove = new ArrayList<>();

				for (int i = cardPlace; i < stacks.get(stackId).getCards().size(); i++) {
					cardsToMove.add(stacks.get(stackId).getCards().get(i));
				}
				selectedCard = stacks.get(stackId).getCards().get(cardPlace).getValue() + " "
						+ stacks.get(stackId).getCards().get(cardPlace).getSuit();
			} else {
				clickedCard = stacks.get(stackId).getCards().get(cardPlace);
				cardToRemove = clickedCard;
			}

		} else {
			stackId = source.substring(2, source.length());

			if (stacks.get(stackId).getCards().size() > 0) {

				clickedCard = stacks.get(stackId).getCards().get(stacks.get(stackId).getCards().size() - 1);
				cardToRemove = clickedCard;
			}
		}

		lastStackId = stackId;

		if (clickedCard != null)
			return clickedCard.getValue() + " " + clickedCard.getSuit();

		return selectedCard;
	}

	public String setSecondClick(MouseEvent event) {
		String source = ((ImageView) event.getSource()).getId();
		String numberParts = this.getNumberParts(source);
		String stackId = source.substring(2, source.length());

		if (!source.contains("Flip"))
			if (!numberParts.equals("")) {
				stackId = source.substring(0, source.length() - numberParts.length());
				stackId = stackId.substring(2, stackId.length());

				if (cardsToMove != null) {
					clickedCard = cardsToMove.get(0);
					
					if (isValidWorth(stackId)) {
						stacks.get(stackId).addCard(cardsToMove);
						stacks.get(lastStackId).removeCard(cardsToMove);
						stacks.get(lastStackId).showCard();
					}

				} else if (isValidWorth(stackId)) {
					stacks.get(stackId).addCard(clickedCard.setShow(true));
					stacks.get(lastStackId).removeCard(clickedCard);
					stacks.get(lastStackId).showCard();
				}
				
			} else if (source.contains("Final")) {
				if (isFinalValidWorth(stackId)) {
					stacks.get(stackId).addCard(clickedCard.setShow(true));
					stacks.get(lastStackId).removeCard(cardToRemove.setShow(true));
					stacks.get(lastStackId).showCard();;
				}

			} else if (stacks.get(stackId).getCards().isEmpty()) {
				if (isValidWorth(stackId)) {
					stacks.get(stackId).addCard(clickedCard.setShow(true));
					stacks.get(lastStackId).removeCard(cardToRemove.setShow(true));
					stacks.get(lastStackId).showCard();
				}
			}
		
		cardsToMove = null;
		clickedCard = null;
		return "";
	}

	/**
	 * Validates if the Clicked card is worth enough for been replaced
	 * 
	 * @param stackId
	 * @return is worth or not
	 */
	private boolean isValidWorth(String stackId) {
		boolean isWorth = false;

		if (clickedCard != null) {
			if (stacks.get(stackId).getCards().isEmpty()) {
				if (clickedCard.getWeight() == 13)
					isWorth = true;
			} else {
				Card comperatorCard = stacks.get(stackId).getCards().get(stacks.get(stackId).getCards().size() - 1);

				if ((comperatorCard.getWeight() - clickedCard.getWeight() == 1)
						&& ((clickedCard.getColorWeight() != comperatorCard.getColorWeight()))) {
					isWorth = true;
				}
			}
		}

		return isWorth;
	}

	/**
	 * Validates if the Clicked card is worth enough for been replaced to the final
	 * Stack
	 * 
	 * @param stackId
	 * @return is worth or not
	 */
	private boolean isFinalValidWorth(String stackId) {
		boolean isWorth = false;

		if (clickedCard != null)
			if (stacks.get(stackId).getCards().isEmpty()) {
				if (clickedCard.getWeight() == 1)
					isWorth = true;
			} else {
				Card comperatorCard = stacks.get(stackId).getCards().get(stacks.get(stackId).getCards().size() - 1);
				if ((clickedCard.getWeight() - comperatorCard.getWeight() == 1)
						&& (clickedCard.getSuit().equals(comperatorCard.getSuit()))) {
					isWorth = true;
				}

			}
		return isWorth;
	}

	private String getNumberParts(String source) {
		Pattern p = Pattern.compile("[0-9]");
		Matcher m = p.matcher(source);

		String numberParts = "";
		while (m.find()) {
			numberParts = numberParts + String.valueOf(m.group());
		}

		return numberParts;
	}

	public boolean gameFinished() {
		if ((stacks.get("FinalStackOne").getCards().size() == 1) && (stacks.get("FinalStackTwo").getCards().size() == 1)
				&& (stacks.get("FinalStackThree").getCards().size() == 1)
				&& (stacks.get("FinalStackFour").getCards().size() == 1))
			return true;
		return false;
	}

}
