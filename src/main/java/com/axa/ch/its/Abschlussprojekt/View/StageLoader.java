package com.axa.ch.its.abschlussprojekt.view;

import java.io.IOException;
import java.net.URL;

import com.axa.ch.its.abschlussprojekt.control.Controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class StageLoader extends Stage {
	private String FXMLFile;
	private String text;
	private Controller nextController;

	public StageLoader(String FXMLFile, String text, Controller nextController) {
		this.FXMLFile = FXMLFile;
		this.text = text;
		this.nextController = nextController;
	}

	public void loadStage() {
		URL url = this.getClass().getResource("/fxml/" + FXMLFile);
		FXMLLoader loader = new FXMLLoader(url);
		loader.setController(nextController);

		Parent root = null;
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.setScene(new Scene(root));
		this.setTitle(text + " by ©Jan Hasler");
		this.setResizable(false);
		this.show();
	}

}
