package com.axa.ch.its.abschlussprojekt.control;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.axa.ch.its.abschlussprojekt.control.logic.SolitaireLogic;
import com.axa.ch.its.abschlussprojekt.model.Card;
import com.axa.ch.its.abschlussprojekt.model.MyObserver;
import com.axa.ch.its.abschlussprojekt.view.StageLoader;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public class SolitaireController extends Controller implements MyObserver<StackModel> {

	private static final Image CARD_BACKSIDE = new Image("/pictures/cardBackside.jpg");
	private static final Image CARD_PLACEHOLDER = new Image("/pictures/placeholder.jpg");

	// FXML Objects
	@FXML
	private Pane root;

	@FXML
	private Button btnBack;
	@FXML
	private Button btnStartNewGame;

	@FXML
	private ImageView ivFinalStackFour;
	@FXML
	private ImageView ivFinalStackThree;
	@FXML
	private ImageView ivFinalStackTwo;
	@FXML
	private ImageView ivFinalStackOnw;
	@FXML
	private ImageView ivBackStack;
	@FXML
	private ImageView ivBackStackFlip;
	@FXML
	private ImageView ivMiddleStackOne;
	@FXML
	private ImageView ivMiddleStackTwo;
	@FXML
	private ImageView ivMiddleStackThree;
	@FXML
	private ImageView ivMiddleStackFour;
	@FXML
	private ImageView ivMiddleStackFive;
	@FXML
	private ImageView ivMiddleStackSix;
	@FXML
	private ImageView ivMiddleStackSeven;

	@FXML
	private Label lblPoints;
	@FXML
	private Label lblTime;
	@FXML
	private Label lblError;
	@FXML
	private Label lblRemaining;
	@FXML
	private Label lblSelectedCard;

	// Other Objects
	private SolitaireLogic logic;

	private Timer timer;
	private boolean firstClick;
	
	private MouseEvent event;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		btnBack.setOnAction(a -> {
			timer.cancel();
			goBack(a);
		});

		timer = new Timer();
		btnStartNewGame.setOnAction(this::startGame);

		logic = new SolitaireLogic(this);

		lblRemaining.setText(logic.getRemainingBackStack());
		firstClick = true;

	}

	private void startGame(ActionEvent event) {
		btnStartNewGame.setDisable(true);
		logic.startGame();

		timer.scheduleAtFixedRate(new TimerTask() {
			private int sec = 0;
			private int min = 0;

			@Override
			public void run() {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						if (sec < 10)
							lblTime.setText("Time: " + min + ":0" + sec);
						else
							lblTime.setText("Time: " + min + ":" + sec);
					}
				});
				sec++;
				if (sec == 60) {
					min++;
					sec = 0;
				}
			}
		}, 0, 1000);

	}

	private void flipCard(final MouseEvent event) {
		logic.flipCard();
		lblRemaining.setText(logic.getRemainingBackStack());
	}

	private void clickCard(final MouseEvent event) {
		if (firstClick)
			lblSelectedCard.setText(logic.setFirstClick(event));
		else {
			lblSelectedCard.setText(logic.setSecondClick(event));
			if (logic.gameFinished()) {
				this.event = event;
				StageLoader gameLoader = new StageLoader("popUpView.fxml", "You have won", new PopUpController(this));
				gameLoader.loadStage();
			}
		}

		firstClick = !firstClick;
	}

	@Override
	public void update(StackModel model) {
		String id = model.getStackId();
		List<Card> cards = model.getCards();
		ImageView ivToUpdate = null;

		// To remove old Views
		removeInvalidImageViews(id);

		// Get the lowest ImageView
		for (Node node : root.getChildren())
			if (node.getId().equals("iv" + id) && node instanceof ImageView) {
				ivToUpdate = (ImageView) node;
			}

		double xCooridnate = ivToUpdate.getLayoutX();
		double yCoordinate = ivToUpdate.getLayoutY();

		double height = ivToUpdate.getFitHeight();
		double width = ivToUpdate.getFitWidth();

		int counter = 0;

		// Update Middlestacks
		if (id.contains("Middle")) {
			if (cards.isEmpty()) {
				ivToUpdate.setImage(CARD_PLACEHOLDER);
				ivToUpdate.setOnMouseClicked(this::clickCard);
				ivToUpdate.setCursor(Cursor.HAND);
			} else
				for (Card card : cards) {
					ImageView newIv = new ImageView();
					newIv.relocate(xCooridnate, yCoordinate);
					newIv.setFitWidth(width);
					newIv.setFitHeight(height);
					newIv.setId("iv" + id + counter);

					if (card.isShown()) {
						newIv.setImage(getCardImage(card.getImage()));
						newIv.setOnMouseClicked(this::clickCard);
						newIv.setCursor(Cursor.HAND);
					} else
						newIv.setImage(CARD_BACKSIDE);

					yCoordinate += 30;
					root.getChildren().add(newIv);

					counter++;
				}

		}
		// Update BackstackFlip and FinalStack
		else if (id.contains("BackStackFlip") || id.contains("Final")) {
			if (cards.isEmpty()) {
				ivToUpdate.setImage(CARD_PLACEHOLDER);
			} else {
				ivToUpdate.setImage(getCardImage(cards.get(cards.size() - 1).getImage()));
			}

			if (id.contains("BackStackFlip") && cards.isEmpty()) {
				ivToUpdate.setOnMouseClicked(null);
				ivToUpdate.setCursor(Cursor.DEFAULT);
			} else {
				ivToUpdate.setOnMouseClicked(this::clickCard);
				ivToUpdate.setCursor(Cursor.HAND);
			}

			// Update Backstack
		} else if (id.equals("BackStack")) {
			if (cards.isEmpty())
				ivToUpdate.setImage(CARD_PLACEHOLDER);
			else
				ivToUpdate.setImage(CARD_BACKSIDE);

			ivToUpdate.setOnMouseClicked(this::flipCard);
		}

	}

	private void removeInvalidImageViews(String id) {
		List<ImageView> ivToRemove = new ArrayList<>();
		Pattern p = Pattern.compile("[0-9]");
		Matcher m = null;
		for (Node node : root.getChildren()) {
			if (node.getId().contains("iv" + id) && node instanceof ImageView) {
				ImageView remove = (ImageView) node;
				m = p.matcher(remove.getId());
				if (m.find()) {
					ivToRemove.add(remove);

				}
			}
		}
		root.getChildren().removeAll(ivToRemove);

	}

	public void toMain(ActionEvent event) {
		super.goBack(event);
		super.closeScene(this.event);
	}

	class PopUpController extends Controller {

		@FXML
		private Button btnYes;
		@FXML
		private Button btnNo;

		private SolitaireController solitaire;

		@Override
		public void initialize(URL location, ResourceBundle resources) {
			btnNo.setOnAction(this::goToMain);
			btnYes.setOnAction(this::restart);

		}

		public PopUpController(SolitaireController solitaire) {
			this.solitaire = solitaire;
		}

		public void restart(ActionEvent event) {
			//TODO restart game
		}

		public void goToMain(ActionEvent event) {
			solitaire.toMain(event);
		}

	}

}
