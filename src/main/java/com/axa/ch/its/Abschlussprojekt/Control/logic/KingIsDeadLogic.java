package com.axa.ch.its.abschlussprojekt.control.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.axa.ch.its.abschlussprojekt.model.Card;
import com.axa.ch.its.abschlussprojekt.service.CardService;

public class KingIsDeadLogic {
	private HashMap<String, String> availableCards;


	private CardService service;

	private String selectionCode;

	private Card currentCard;

	public KingIsDeadLogic() {
		service = new CardService();
		loadAllAvailableCards();
	}

	public Card flipCard() {
		currentCard = service.flipCard();	
		availableCards.remove(currentCard.getCode());
		
		if(availableCards.isEmpty()) 						
			throw new IndexOutOfBoundsException();
		
		

		return currentCard;
	}

	public boolean cardFound() {
		return currentCard.getCode().contains(selectionCode);
	}

	public void loadAllAvailableCards() {
		// Cardcode + full qualified name
		HashMap<String, String> availableCards = new HashMap<>();
		List<String> suits = new ArrayList<>();
		List<String> values = new ArrayList<>();

		suits = Arrays.asList("King", "Queen", "Jack", "10", "9", "8", "7", "6", "5", "4", "3", "2", "ACE");
		values = Arrays.asList("DIAMONDS", "CLUBS", "SPADES", "HEARTS");

		for (String suit : suits) {
			for (String value : values) {
				String code = (suit.equals("10") ? "0" : suit.substring(0, 1)) + value.substring(0, 1);

				availableCards.put(code, suit + " " + value);
			}
		}


		this.availableCards = availableCards;
	}

	public void setSelection(String selection) {
		String[] split = selection.split(" ");
		this.selectionCode = (split[0].equals("10") ? "0" : split[0]) + split[1].substring(0, 1);

	}

	public void newDeck() {
		service.getNewDeck();
		loadAllAvailableCards();
		selectionCode = "";

	}

	public HashMap<String, String> getAvailableCards() {
		return availableCards;
	}
}
