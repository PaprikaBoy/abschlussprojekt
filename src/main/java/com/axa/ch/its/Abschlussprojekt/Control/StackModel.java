package com.axa.ch.its.abschlussprojekt.control;

import java.util.ArrayList;
import java.util.List;

import com.axa.ch.its.abschlussprojekt.model.Card;
import com.axa.ch.its.abschlussprojekt.model.MyObservable;
import com.axa.ch.its.abschlussprojekt.model.MyObserver;

public class StackModel implements MyObservable<MyObserver<StackModel>> {
	private List<MyObserver<StackModel>> observers;

	private String stackId;
	private List<Card> cards;

	public StackModel(String stackId) {
		observers = new ArrayList<>();
		cards = new ArrayList<>();
		this.stackId = stackId;
	}

	public void updateStack() {
		this.notifyObservers();
	}

	@Override
	public void addObserver(MyObserver<StackModel> observer) {
		observers.add(observer);
		observer.update(this);
	}

	private void notifyObservers() {
		observers.forEach(o -> o.update(this));
	}

	public String getStackId() {
		return stackId;
	}

	public void setStackId(String stackId) {
		this.stackId = stackId;
	}

	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

	public void addCard(Card card) {
		this.cards.add(card);
		this.notifyObservers();
	}

	public void addCard(List<Card> cards) {
		for (Card card : cards) {
			this.cards.add(card);
		}
		this.notifyObservers();
	}

	public void removeCard(int cardPlace) {
		this.cards.remove(cardPlace);
		this.notifyObservers();

	}

	public void removeCard(List<Card> cards) {
		for (Card card : cards) {
			this.cards.remove(card);
		}
		this.notifyObservers();
	}

	public void removeCard(Card cardToRemove) {
		int place = -1;
		for (int i = 0; i < cards.size(); i++) {
			if (cardToRemove.getCode().equals(cards.get(i).getCode())) {
				place = i;
				break;
			}

		}

		this.cards.remove(place);
		this.notifyObservers();
	}

	public void removeAllCards() {
		this.cards.clear();
		this.notifyObservers();
	}

	public void addSomeCards(List<Card> cards) {
		this.cards.addAll(cards);
		this.notifyObservers();
	}

	public void showCard(Card cardToRemove) {
		int place = -1;
		for (int i = 0; i < cards.size(); i++) {
			if (cardToRemove.getCode().equals(cards.get(i).getCode())) {
				place = i;
				break;
			}

		}

		this.cards.get(place).setShow(true);
		this.notifyObservers();
	}

	public void showCard() {
		if (!cards.isEmpty())
			this.cards.get(cards.size() - 1).setShow(true);
		this.notifyObservers();
	}

}
