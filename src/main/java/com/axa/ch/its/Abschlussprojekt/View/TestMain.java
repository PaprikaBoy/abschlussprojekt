package com.axa.ch.its.abschlussprojekt.view;

import java.io.FileNotFoundException;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class TestMain extends Application {
	private double orgSceneX;
	private double orgSceneY;
	private double orgTranslateX;
	private double orgTranslateY;
	double y;
	double x;

	@Override
	public void start(Stage stage) throws FileNotFoundException {
		Pane root = new Pane();
		Scene scene = new Scene(root, 300, 300);

		ImageView iv = new ImageView(new Image("/pictures/placeholder.jpg"));
		iv.setOnMouseDragged(circleOnMouseDraggedEventHandler);
		iv.setOnMousePressed(circleOnMousePressedEventHandler);

		root.getChildren().add(iv);

		stage.setTitle("Moving ImageView Overlay Test");
		stage.setScene(scene);
		stage.show();
	}

	EventHandler<MouseEvent> circleOnMousePressedEventHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent t) {
			orgSceneX = t.getSceneX();
			orgSceneY = t.getSceneY();
			orgTranslateX = ((ImageView) (t.getSource())).getTranslateX();
			orgTranslateY = ((ImageView) (t.getSource())).getTranslateY();
		}
	};

	EventHandler<MouseEvent> circleOnMouseDraggedEventHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent t) {
			double offsetX = t.getSceneX() - orgSceneX;
			double offsetY = t.getSceneY() - orgSceneY;
			double newTranslateX = orgTranslateX + offsetX;
			double newTranslateY = orgTranslateY + offsetY;

			((ImageView) (t.getSource())).setTranslateX(newTranslateX);
			((ImageView) (t.getSource())).setTranslateY(newTranslateY);
		}
	};

	/**
	 * The main() method is ignored in correctly deployed JavaFX application. main()
	 * serves only as fallback in case the application can not be launched through
	 * deployment artifacts, e.g., in IDEs with limited FX support. NetBeans ignores
	 * main().
	 *
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
		System.out.println("ivViewLOl".substring(2));
	}
}
