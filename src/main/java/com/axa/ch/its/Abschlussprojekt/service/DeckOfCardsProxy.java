package com.axa.ch.its.abschlussprojekt.service;

import java.io.Serializable;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import com.axa.ch.its.abschlussprojekt.model.Deck;

public class DeckOfCardsProxy implements Serializable{
	private static final long serialVersionUID = 4118049466557505880L;
	
	private Client client;
	
	public DeckOfCardsProxy() {
		client = ClientBuilder.newClient();
	}
	
	public Deck getNewDeck() {
		Deck deck = new Deck();
		WebTarget target;
		
		target = client.target("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1");

		deck = target.request(MediaType.APPLICATION_JSON).get(Deck.class);
		String deckId = deck.getDeck_id();
		
		//laod all card of the Deck
		try {
			target = client.target("https://deckofcardsapi.com/api/deck/" + deckId + "/draw/?count=52");
			deck = target.request(MediaType.APPLICATION_JSON).get(Deck.class);
		} catch (Exception e) {
			return null;
		}
		
		return deck;
	}

}
