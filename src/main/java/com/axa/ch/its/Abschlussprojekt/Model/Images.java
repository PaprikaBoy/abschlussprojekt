package com.axa.ch.its.abschlussprojekt.model;

import java.net.URL;

public class Images {
	private URL png;
	private URL svg;
	
	public URL getPng() {
		return png;
	}
	public void setPng(URL png) {
		this.png = png;
	}
	public URL getSvg() {
		return svg;
	}
	public void setSvg(URL svg) {
		this.svg = svg;
	}
	
}
