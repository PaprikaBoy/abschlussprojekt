package com.axa.ch.its.abschlussprojekt.control;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import com.axa.ch.its.abschlussprojekt.view.StageLoader;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;

public class MainController extends Controller{
	@FXML
	private Button btnStartGame;
	@FXML
	private ComboBox<String> ddGames;
	@FXML
	private Label lblError;

	private HashMap<String, StageLoader> stages;

	public MainController() {
		stages = new HashMap<>();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		btnStartGame.setOnAction(this::loadGame);
		btnStartGame.setCursor(Cursor.HAND);
		
		ddGames.setItems(addGames());
		ddGames.setCursor(Cursor.HAND);
	}

	private void loadGame(ActionEvent event) {
		String selectedGame = ddGames.getSelectionModel().getSelectedItem();
		
		if (selectedGame == null)
			lblError.setText("Please select a Game first");
		else {
			stages.get(selectedGame).loadStage();
			
			super.closeScene(event);		
		}

	}

	private ObservableList<String> addGames() {
		ObservableList<String> list = FXCollections.observableArrayList();

		stages.put("Solitaire", new StageLoader("SolitaireView.fxml", "Solitaire", new SolitaireController()));
		stages.put("The King is Dead", new StageLoader("KingIsDeadView.fxml", "The King is Dead", new KingIsDeadController()));
		
		stages.forEach((k, v) -> list.add(k));

		return list;
	}

}
