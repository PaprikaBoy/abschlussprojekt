package com.axa.ch.its.abschlussprojekt.model;

public interface MyObservable<T> {
	
	public void addObserver(T observer);

}