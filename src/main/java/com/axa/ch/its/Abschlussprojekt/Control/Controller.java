package com.axa.ch.its.abschlussprojekt.control;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ResourceBundle;

import com.axa.ch.its.abschlussprojekt.view.StageLoader;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public abstract class Controller implements Initializable {
	
	protected void closeScene(Event event) {
		Node source = (Node) event.getSource();
		Stage stage = (Stage) source.getScene().getWindow();
		stage.close();
	}
	
	protected void goBack(ActionEvent event) {
		new StageLoader("MainView.fxml", "Card Game Main", new MainController()).loadStage();
		closeScene(event);
	}

	@Override
	public abstract void initialize(URL location, ResourceBundle resources);
	
	protected Image getCardImage(URL url) {
		try {
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("User-Agent", "Wget/1.13.4 (linux-gnu)");
			InputStream stream = conn.getInputStream();
			Image img = new Image(stream);
			
			return img;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	

}
